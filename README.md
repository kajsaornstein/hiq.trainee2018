# Trainee 2018

## Exercises/JS

### Install Dependencies

Run commands from the `js` folder.
```bash
yarn
```
or
```bash
npm install
```

### Make Exercises

Open `app.js` and follow the instructions.

### Validate Answers

Open `app.test.js` and uncomment the corresponding test.

Run commands from the `js` folder.
```bash
yarn test
```
or
```bash
npm test
```

---

## Exercises/React

### Setup a simple React project

Install create-react-app
```bash
npm install -g create-react-app
```

Create a new project (without TypeScript)
```bash
create-react-app my-app
```

Create a new project (with TypeScript)
```bash
create-react-app my-app --scripts-version=react-scripts-ts
```

Run the project
```bash
cd my-app
npm run start
```
