/**
 * EXERCISES JAVASCRIPT (ES6)
 *
 * These exercises are created for you to learn more about and practice
 * your javascript skills. For some of you it might be easy, and for some
 * of you a bit of a challenge. If you get stuck, don't hesitate to ask
 * your team members. But first, take a look at
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript
 *
 * We might ask you to write a function on one line. This is primarily
 * to encourage you to practice writing ES6 arrow functions. One liners
 * can make the code cleaner and easier to read, but definately not always.
 * We recommend you to try both and decide what you feel is better.
 *
 * Take your time to read through pieces of the Airbnb style guide.
 * https://github.com/airbnb/javascript
 */

 /**
 * Exersice 0
 *
 * Ever worked with a loosely typed language? Practice your
 * understanding of JavaScript types by answering these questions.
 * Put your answers into the variable ANSWER0.
 *
 * What is the value of x in ...
 * A: x = "10" + 5?
 * B: x = "10" - 5?
 * C: x = "10" * 5?
 * D: x = 10 + "a"?
 * E: x = 10 - "a"?
 * F: x = 10 * "a"?
 * G: x = 10 === "10"?
 * H: x = 10 == "10"?
 * I: x = 10 !== "10"?
 * J: x = 10 != "10"?
 * K: x = 0 === ""?
 * L: x = 0 == ""?
 * M: x = 1 == true?
 * N: x = 1 === true?
 */

let ANSWER0 = {
    A: '',
    B: '',
    C: '',
    D: '',
    E: '',
    F: '',
    G: '',
    H: '',
    I: '',
    J: '',
    K: '',
    L: '',
    M: '',
    N: ''
};

/**
 * Exercise 1
 *
 * Create a function that takes two numbers and
 * returns the sum of these, rounded to two decimals.
 * Extra: Make this a one-line function.
 *
 * Call your function with values 1.234 and 5.678
 * and put the answer into the variable ANSWER1.
 */

const ANSWER1 = '';

/**
 * Exercise 2
 *
 * Create a function that takes an array of numbers and
 * returns an array with the odd numbers of the original array.
 * Extra: Make this a one-line function.
 *
 * Call your function with the array [1, 2, 3, 4, 5, 6, 7, 8, 9]
 * and put the answer into the variable ANSWER2.
 */

const ANSWER2 = '';

/**
 * Exercise 3
 *
 * Create a function that takes an array of numbers and
 * returns an array with the same numbers added by 10.
 * Extra: Make this a one-line function.
 *
 * Call your function with the array [1, 2, 3, 4, 5, 6, 7, 8, 9]
 * and put the answer into the variable ANSWER3.
 */

const ANSWER3 = '';

/**
 * Exercise 4
 *
 * You are provided with an array of object literals representing employees.
 * Create a function that takes an array of employee literals and returns an
 * array with only their names.
 * Extra: Make this a one-line function.
 *
 * Call your function with the provided employees array
 * and put the answer into the variable ANSWER4.
 */

const ALICE = {
    name: "Alice",
    experience: 3,
    team: "Backend",
    competences: ["Java", "MySQL", "Hibernate"]
};

const BEN = {
    name: "Ben",
    experience: 1,
    team: "Frontend",
    competences: ["Angular2", "TypeScript", "Vue.js"]
};

const CASPER = {
    name: "Casper",
    experience: 5,
    team: "Web Development",
    competences: ["React.js", "TypeScript", "MongoDB"]
};

const employees = [ALICE, BEN, CASPER];

const ANSWER4 = '';

 /**
  * Exercise 5
  *
  * Create a function that takes an array of employee literals and a number x.
  * The function should return an array with the names of the employees with
  * at least x experience.
  * Extra: Make this a one-line function.
  *
  * Call your function with the provided employees array and x = 3.
  * Put the answer into the variable ANSWER5.
  */

const ANSWER5 = '';

/**
 * Exercise 6
 *
 * Create a function that takes an array of employee literals and a string x.
 * The function should return an array with the names of the employees in team x.
 * Extra: Make this a one-line function.
 *
 * Call your function with the provided employees array and x = "Frontend".
 * Put the answer into the variable ANSWER6.
 */

const ANSWER6 = '';

/**
 * Exercise 7
 *
 * Create a function that takes an array of employee literals and a string x.
 * The function should return an array with the names of the employees that has
 * experience working with technique x.
 * Extra: Make this a one-line function.
 *
 * Call your function with the provided employees array and x = "TypeScript".
 * Put the answer into the variable ANSWER7.
 */

const ANSWER7 = '';

/**
 * Exercise 8
 *
 * Create a function that takes an array of employee literals and
 * returns the sum of all the employees' experience.
 * Extra: Make this a one-line function.
 *
 * Put the answer into the variable ANSWER8.
 */

const ANSWER8 = '';

/**
 * Exercise 9
 *
 * Create a function that takes an array of employee literals and
 * returns the array sorted by experience in descending order.
 * Extra: Make this a one-line function.
 *
 * Call your function with the provided employees array.
 * Put the answer into the variable ANSWER9.
 */

const ANSWER9 = '';

/**
 * Exercise 10
 *
 * Create a function that takes an array of employee literals and
 * returns the array sorted by names in descending order.
 * Extra: Make this a one-line function.
 *
 * Call your function with the provided employees array.
 * Put the answer into the variable ANSWER10.
 */

const ANSWER10 = '';

module.exports = {
    ANSWER0, ANSWER1, ANSWER2, ANSWER3, ANSWER4,
    ANSWER5, ANSWER6, ANSWER7, ANSWER8, ANSWER9,
    ANSWER10, ALICE, BEN, CASPER };
