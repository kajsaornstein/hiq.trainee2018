const app = require('./app');

// test('Exercise 0', () => {
//     const EXPECTED0 = {
//         A: "10" + 5,
//         B: "10" - 5,
//         C: "10" * 5,
//         D: 10 + "a",
//         E: 10 - "a",
//         F: 10 * "a",
//         G: 10 === "10",
//         H: 10 == "10",
//         I: 10 !== "10",
//         J: 10 != "10",
//         K: 0 === "",
//         L: 0 == "",
//         M: 1 == true,
//         N: 1 === true
//     };
//     expect(app.ANSWER0).toEqual(EXPECTED0);
// });

// test('Exercise 1', () => {
//     expect(app.ANSWER1).toBe(6.91);
// });

// test('Exercise 2', () => {
//       expect(app.ANSWER2).toEqual([1, 3, 5, 7, 9]);
// });

// test('Exercise 3', () => {
//       expect(app.ANSWER3).toEqual([11, 12, 13, 14, 15, 16, 17, 18, 19]);
// });

// test('Exercise 4', () => {
//       expect(app.ANSWER4).toEqual([app.ALICE.name, app.BEN.name, app.CASPER.name]);
// });

// test('Exercise 5', () => {
//     expect(app.ANSWER5).toEqual([app.ALICE.name, app.CASPER.name]);
// });

// test('Exercise 6', () => {
//     expect(app.ANSWER6).toEqual([app.BEN.name]);
// });

// test('Exercise 7', () => {
//     expect(app.ANSWER7).toEqual([app.BEN.name, app.CASPER.name]);
// });

// test('Exercise 8', () => {
//     expect(app.ANSWER8).toEqual(9);
// });

// test('Exercise 9', () => {
//     expect(app.ANSWER9).toEqual([app.CASPER, app.ALICE, app.BEN]);
// });

// test('Exercise 10', () => {
//     expect(app.ANSWER10).toEqual([app.CASPER, app.BEN, app.ALICE]);
// });
